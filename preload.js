// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.ipc = require('electron').ipcRenderer;

window.addChild = function(text) {
  const e = document.createElement('li');
  e.innerHTML = text;
  document.getElementById('list').append(e);
}

window.changeId = function(id) {
  document.getElementById('browser-id').innerHTML = id;
}

function filterByText(list, text) {
  [].filter.call(list, element => {
    return element.innerText.toLowerCase().includes(text);
  }).map(element => {
    return {
      element,
      html: element.outerHTML,
      text: element.text,
      href: element.href,
      geometry: {
        top: element.offsetTop,
        left: element.offsetLeft,
        height: element.offsetHeight,
        width: element.offsetWidth,
      },
    }
  })
}

console.log('aaaaa');

window.execCommand = function(cmd) {
  switch(cmd.data.type) {
    case 'findByText':
      const links        = filterByText(document.links, cmd.data.text);
      const buttons      = filterByText(document.getElementsByTagName('button'), cmd.data.text);
      const buttonInputs = filterByText(document.querySelectorAll('input[type="button"]'), cmd.data.text);
      const submitInputs = filterByText(document.querySelectorAll('input[type="submit"]'), cmd.data.text);
      ipc.send('foundByText', { links, buttons, buttonInputs, submitInputs } )
    default:
      ipc.send('unknownType', cmd.data.type);
  }
}
