// Modules to control application life and create native browser window
const {app, BrowserWindow} = require('electron')
const path = require('path')
const { ipcMain } = require('electron');

const axios = require('axios');

const remote = 'http://localhost:4114';
const state = {};

const mapValues = require('lodash/mapValues');
const omit = require('lodash/omit');

async function startPolling(w, sendCommand) {
  const { data: {id} } = await axios.post(`${remote}/yandex/browser`, {});
  w.webContents.executeJavaScript(`window.changeId('${+id}')`);
  var pollingInterval = setInterval(async function() {
    try {
      const { data: {commands} } = await axios.get(`${remote}/yandex/browser/${id}`);
      commands.forEach(c => sendCommand(c));
    } catch(err) {
      console.log(err);
    }
  }, 1000);
}


function createWindow () {
  // Create the browser window.
  const mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: false,
      enableRemoteModule: false,
    },
  })

  ipcMain.on('cmd', (event, arg) => {
    console.log('ipc', 'cmd', arg);
  })
  ipcMain.on('foundByText', async function (event, arg) {
    state.lastFound = arg;
    axios.post(`${remote}/yandex/browser/answer`, {
      data: {
        type: 'foundByText',
        elements: mapValues(arg, vs => vs.map(v => omit(v, 'element')))
      }
    })
  })
  // and load the index.html of the app.
  mainWindow.loadFile('index.html')

  // Open the DevTools.
  mainWindow.webContents.openDevTools()

  const sendCommand = command => {
    if (command.data.type === 'visit') {
      let visitCode = `window.location = "${command.data.url}"`;
      mainWindow.webContents.executeJavaScript(visitCode);
    }
    const src = JSON.stringify(command);
    const code = `execCommand(${src})`;
    mainWindow.webContents.executeJavaScript(code);
  }
  startPolling(mainWindow, sendCommand);
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On macOS it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (BrowserWindow.getAllWindows().length === 0) createWindow()
})

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
